#!/bin/bash

# Simple wrapper to set up a traditional ROSA cluster
# Author: Neal Gompa <neal@gompa.dev>
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

clustercreatesh="$(basename "$0")"

usage() {
	echo >&2 "usage: $clustercreatesh --cluster-name=NAME [--cluster-version=VERSION] --aws-region=REGION --aws-acct=ACCTID [--aws-tags=TAGS] [--debug]"
	echo >&2 "   eg: $clustercreatesh --cluster-name=rosacluster --cluster-version=4.14.3 --aws-region=us-east-2 --aws-acct=000000000000 --debug"
	echo >&2 "   eg: $clustercreatesh --cluster-name=rosacluster --cluster-version=4.14.3 --aws-region=us-east-2 --aws-acct=000000000000"
	exit 255
}

optTemp=$(getopt --options '+n:,v:,r:,a:,t:,d,h' --longoptions 'cluster-name:,cluster-version:,aws-region:,aws-acct:,aws-tags:,debug,help' --name "$clustercreatesh" -- "$@")
eval set -- "$optTemp"
unset optTemp

cluster_name=
cluster_version=
aws_region=
aws_acct=
aws_tags=
debug=

while true; do
	case "$1" in
		-n|--cluster-name) cluster_name="$2" ; shift 2 ;;
		-v|--cluster-version) cluster_version="$2" ; shift 2 ;;
		-r|--aws-region) aws_region="$2" ; shift 2 ;;
		-a|--aws-acct) aws_acct="$2" ; shift 2 ;;
		-t|--aws-tags) aws_tags="$2" ; shift 2 ;;
		-d|--debug) debug="yes" ; shift ;;
		-h|--help) usage ;;
		--) shift ; break ;;
	esac
done


export PATH=$HOME/.local/bin:$PATH

if [ -z "$cluster_name" ] || [ -z "$aws_region" ] || [ -z "$aws_acct" ]; then
	echo "Options not set!"
	exit 1
fi

if [ "${#cluster_name}" -gt 15 ]; then
	echo "Cluster name too long (>15 characters), quitting!"
	exit 2
fi

if [ "${#aws_acct}" -ne 12 ]; then
	echo "Account ID number not the right length (12 digits), quitting!"
	exit 3
fi

if [ "$debug" = "yes" ]; then
	set -x
fi

CLUSTER_ROLE_PREFIX_RANDSUFFIX=$(date | md5sum | head -c 4 ; echo '')

# Create cluster
echo "Creating ROSA cluster..."

rosa create cluster \
	--cluster-name "${cluster_name}" --sts \
	--role-arn "arn:aws:iam::${aws_acct}:role/ManagedOpenShift-Installer-Role" \
	--support-role-arn "arn:aws:iam::${aws_acct}:role/ManagedOpenShift-Support-Role" \
	--controlplane-iam-role "arn:aws:iam::${aws_acct}:role/ManagedOpenShift-ControlPlane-Role" \
	--worker-iam-role "arn:aws:iam::${aws_acct}:role/ManagedOpenShift-Worker-Role" \
	--operator-roles-prefix "${cluster_name}-${CLUSTER_ROLE_PREFIX_RANDSUFFIX}" ${aws_tags:+--tags "${aws_tags}"} \
	--region "${aws_region}" ${cluster_version:+--version "${cluster_version}"} --replicas 2 --compute-machine-type m6a.xlarge \
	--machine-cidr "10.0.0.0/16" --service-cidr "172.30.0.0/16" --pod-cidr "10.128.0.0/14" --host-prefix 23

# Prepare to create roles and provider
echo "Waiting for cluster creation to start..."
sleep 10

# Create operator roles
rosa create operator-roles --cluster "${cluster_name}" --mode auto --yes

# Create OIDC provider
rosa create oidc-provider --cluster "${cluster_name}" --mode auto --yes
