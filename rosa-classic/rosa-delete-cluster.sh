#!/bin/bash

# Simple wrapper to tear down a traditional ROSA cluster
# Author: Neal Gompa <neal@gompa.dev>
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

clusterdeletesh="$(basename "$0")"

usage() {
	echo >&2 "usage: $clusterdeletesh --cluster-name=NAME [--debug]"
	echo >&2 "   eg: $clusterdeletesh --cluster-name=rosacluster --debug"
	echo >&2 "   eg: $clusterdeletesh --cluster-name=rosacluster"
	exit 255
}

optTemp=$(getopt --options '+n:,d,h' --longoptions 'cluster-name:,debug,help' --name "$clusterdeletesh" -- "$@")
eval set -- "$optTemp"
unset optTemp

cluster_name=
debug=

while true; do
	case "$1" in
		-n|--cluster-name) cluster_name="$2" ; shift 2 ;;
		-d|--debug) debug="yes" ; shift ;;
		-h|--help) usage ;;
		--) shift ; break ;;
	esac
done

export PATH=$HOME/.local/bin:$PATH

if [ -z "$cluster_name" ]; then
	echo "Options not set!"
	exit 1
fi

if [ "${#cluster_name}" -gt 15 ]; then
	echo "Cluster name too long (>15 characters), quitting!"
	exit 2
fi

if [ "$debug" = "yes" ]; then
	set -x
fi

# Delete cluster
rosa delete cluster --cluster "${cluster_name}" --yes

# Delete operator roles
rosa delete operator-roles --cluster "${cluster_name}" --mode auto --yes

# Delete OIDC provider
rosa delete oidc-provider --cluster "${cluster_name}" --mode auto --yes
