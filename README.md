# ROSA Setup Scripts

This repository contains basic scripts for lifecycling [Red Hat OpenShift Service on AWS](https://www.redhat.com/en/technologies/cloud-computing/openshift/aws) clusters.

## Prerequisites

* AWS CLI v2
* OpenShift Clients
* ROSA CLI

## Licensing

These scripts are licensed under the Apache Software License, version 2.0. See `LICENSE` for details.
